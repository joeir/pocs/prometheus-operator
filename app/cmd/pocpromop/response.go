package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type response struct {
	Message string      `json:"message"`
	OK      bool        `json:"ok"`
	Data    interface{} `json:"data,omitempty"`
}

func (r *response) Send(
	writer http.ResponseWriter,
	statusCode int,
	message string,
	vars ...interface{},
) {
	r.OK = statusCode < 400
	writer.WriteHeader(statusCode)
	r.Message = message
	responseJSON, marshalError := json.MarshalIndent(r, "", "  ")
	if marshalError != nil {
		fmt.Fprintf(writer, `{"message":"failed to marshal json: %s",ok:false}`)
		return
	}
	fmt.Fprintf(writer, string(responseJSON), vars...)
}
