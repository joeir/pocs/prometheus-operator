package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/google/uuid"
)

func WebhookHandler(w http.ResponseWriter, r *http.Request) {
	requestId := uuid.New().String()
	username, password, _ := r.BasicAuth()
	cookies := map[string]interface{}{}
	for _, cookie := range r.Cookies() {
		cookies[cookie.Name] = cookie.Value
	}
	body, readAllError := ioutil.ReadAll(r.Body)
	if readAllError != nil {
		body = []byte(fmt.Sprintf("error: %s", readAllError))
	}
	request := map[string]interface{}{
		"requestUuid": requestId,
		"cookies":     cookies,
		"headers":     r.Header,
		"remoteAddr":  r.RemoteAddr,
		"protocol":    r.Proto,
		"authentication": map[string]interface{}{
			"username": username,
			"password": password,
		},
		"hostname": r.Host,
		"path":     r.URL.RawPath,
		"query":    r.URL.RawQuery,
		"body":     string(body),
	}
	requestAsJSON, marhsalJSONError := json.MarshalIndent(request, "", "  ")
	if marhsalJSONError != nil {
		log.Errorf("failed to marshal request: %s", marhsalJSONError)
		res := response{Data: marhsalJSONError}
		res.Send(w, http.StatusBadRequest, "failed to marshal json")
		return
	}
	log.Infof("received request as follows: %s", string(requestAsJSON))
	res := response{Data: request}
	res.Send(w, http.StatusOK, "received request")
}
