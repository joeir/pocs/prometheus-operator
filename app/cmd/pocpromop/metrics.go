package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	customMetricA = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "custom_fixed_metric_a",
		Help: "this is a fixed custom metric a that is set via http call to /a/{value}",
	})
	customMetricB = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "custom_fixed_metric_b",
		Help: "this is a fixed custom metric b that is set via http call to /b/{value}",
	})
	customMetricC = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "custom_fixed_metric_c",
		Help: "this is a fixed custom metric c that is set via http call to /c/{value}",
	})
	metricForTeam0 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_0",
		Help: "this metric should be linked to team 0 in the prometheus rules (set via http call to /0/{value})",
	})
	metricForTeam1 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_1",
		Help: "this metric should be linked to team 1 in the prometheus rules (set via http call to /1/{value})",
	})
	metricForTeam2 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_2",
		Help: "this metric should be linked to team 2 in the prometheus rules (set via http call to /2/{value})",
	})
	metricForTeam3 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_3",
		Help: "this metric should be linked to team 3 in the prometheus rules (set via http call to /3/{value})",
	})
	metricForTeam4 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_4",
		Help: "this metric should be linked to team 4 in the prometheus rules (set via http call to /4/{value})",
	})
	metricForTeam5 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_5",
		Help: "this metric should be linked to team 5 in the prometheus rules (set via http call to /5/{value})",
	})
	metricForTeam6 = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "team_metric_6",
		Help: "this metric should be linked to team 6 in the prometheus rules (set via http call to /6/{value})",
	})
)
