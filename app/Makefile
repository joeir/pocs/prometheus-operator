CMD_NAME=pocpromop
GO_PACKAGE_URL=$(shell cat go.mod | head -n 1 | cut -f 2 -d ' ')
DOCKER_IMAGE_NAMESPACE=stashaway
DOCKER_TARBALL_PATH=./output
GIT_REPO_URL=$(shell git remote -v | grep origin | grep push | tr -s '\t' ' ' | cut -d ' ' -f 2)
GIT_COMMIT=$(shell git rev-parse --verify HEAD)
GIT_TAG=$(shell git describe --tag $(shell git rev-list --tags --max-count=1))
MAINTAINER=stashaway
TIMESTAMP=$(shell date +'%Y%m%d%H%M%S')

binary:
	go build -mod=vendor ${EXTRA_GO_FLAGS} -v -x \
		-ldflags "\
			${EXTRA_LD_FLAGS} \
			-X $(GO_PACKAGE_URL)/internal/constants.Commit=$(GIT_COMMIT) \
			-X $(GO_PACKAGE_URL)/internal/constants.Semver=$(GIT_TAG) \
			-X $(GO_PACKAGE_URL)/internal/constants.Timestamp=$(TIMESTAMP) \
			-s -w \
		" \
		-o ./bin/$(CMD_NAME)_$(shell go env GOOS)_$(shell go env GOARCH) \
		./cmd/$(CMD_NAME)
	shasum -a 256 ./bin/$(CMD_NAME)_$(shell go env GOOS)_$(shell go env GOARCH) \
		> ./bin/$(CMD_NAME)_$(shell go env GOOS)_$(shell go env GOARCH).sha256

run:
	go run ./cmd/${CMD_NAME}

coverage:
	go test -v ./... -coverpkg=./... -coverprofile coverage.out
	go tool cover -func ./coverage.out

deps:
	go mod vendor -v
	go mod tidy -v

image:
	docker build \
		--build-arg GIT_REPO_URL=$(GIT_REPO_URL) \
		--build-arg GIT_COMMIT=$(GIT_COMMIT) \
		--build-arg GIT_TAG=$(GIT_TAG) \
		--build-arg GO_PACKAGE_URL=$(GO_PACKAGE_URL) \
		--build-arg TIMESTAMP=$(TIMESTAMP) \
		--build-arg MAINTAINER=$(MAINTAINER) \
		--build-arg CMD_NAME=$(CMD_NAME) \
		--file ./build/Dockerfile \
		--tag $(DOCKER_IMAGE_NAMESPACE)/${CMD_NAME}:latest \
		--target production \
		.
