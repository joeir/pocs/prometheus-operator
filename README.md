# POC: Prometheus Operator

This repository provides a test Prometheus deployment for experimenting with various alert triggers.

- [POC: Prometheus Operator](#poc-prometheus-operator)
  - [Setting Up](#setting-up)
    - [The Kubernetes Cluster](#the-kubernetes-cluster)
    - [Prometheus Operator](#prometheus-operator)
    - [Prometheus Service](#prometheus-service)
    - [Example Alerts](#example-alerts)
    - [Example Application](#example-application)
    - [Expose services locally](#expose-services-locally)
    - [Success State Verification](#success-state-verification)
  - [Things to do](#things-to-do)
    - [Triggering a custom metric alert](#triggering-a-custom-metric-alert)
    - [Triggering a team alert](#triggering-a-team-alert)
  - [Integration Guides](#integration-guides)
    - [Integrating OpsGenie](#integrating-opsgenie)
  - [Common Issues](#common-issues)
    - [ServiceMonitor not showing in Targets page](#servicemonitor-not-showing-in-targets-page)
    - [PrometheusRule - `groupname is repeated in the same file`](#prometheusrule---groupname-is-repeated-in-the-same-file)
    - [AlertManager not sending alert](#alertmanager-not-sending-alert)
    - [OpsGenie alert not created](#opsgenie-alert-not-created)
  - [References](#references)
- [Licensing](#licensing)

## Setting Up

> Run `make precheck` to confirm you have required system dependencies. Also, the following steps should be followed sequentially.

### The Kubernetes Cluster

We use [Kubernetes-in-Docker (KIND)](https://kind.sigs.k8s.io/) to do this. Ensure you have KIND installed and run:

```sh
make up
```

A local development cluster should be available on your machine via Docker.

Run `kubectl config current-context` to verify you are using the correct context. It should be `kind-pocpromop` (or whatever the `NAME` variable is set to in the Makefile)

> Run `make down` to kill the cluster

### Prometheus Operator

This repository uses Git submodules to store the Prometheus Operator code.

Run `git submodule init` to initilaise the Git submodule.

Run `make prometheus_operator` to bring up the Prometheus Operator.

> Run `make prometheus_operator_down` to bring down the Prometheus Operator.

### Prometheus Service

> Make a copy of the file at `./config/alertmanager.sample.yaml` as `./config/alertmanager.yaml` and fill up the required secrets as desired. Examples of integrations you can include can be found in the [Integration Guide section](#integration-guides)

Run `make prometheus` to bring up the Prometheus and AlertManager services. Run `make prometheus_down` to bring down these services

### Example Alerts

> This step is optional and simply generates two alerts, one that is firing, one that is not

Run `make alerts` to set up example alerts for triggering a flow through Prometheus and AlertManager and run `make alerts_down` to remove the example alerts.

### Example Application

> The example application provides a `/metrics` endpoint that can be scraped by Prometheus, and also a `/` endpoint that can receive and debug webhooks from the AlertManager. The [section on Things to Do](#things-to-do) details how you can use this `example-app`

Run `make example_app` to set up the example application and `make example_app_down` to tear it down

### Expose services locally

Finally, run `make local` to expose the three services locally:
1. Prometheus at [http://localhost:9090](http://localhost:9090)
2. AlertManager at [http://localhost:9093](http://localhost:9093)
3. Example App at [http://localhost:4321](http://localhost:4321)

Access Prometheus to view the metrics/do manual querying.

Access AlertManager to view the fired alerts.

Access the Example App to view the raw metrics/trigger alerts.

### Success State Verification

To ensure you're fully set up, you may run the following and check for errors (non-zero exit codes - check with `echo $?` after the command completes):

```sh
# verify that kind has created the cluster
kind get clusters | grep pocpromop;

# verify the prometheus operator is deployed
kubectl get pods --all-namespaces | grep prometheus-operator-;

# verify the prometheus server is deployed
kubectl get pods --all-namespaces | grep prometheus-prometheus;

# verify the alertmanager service is deployed
kubectl get pods --all-namespaces | grep alertmanager-alertmanager;

# verify the application is fully deployed
kubectl get pods --all-namespaces | grep example-app;

# verify the service monitor is picked up
kubectl get secret prometheus-prometheus -o json | jq -r '.data["prometheus.yaml.gz"]' | base64 -d | gunzip | grep example-app
```

## Things to do

### Triggering a custom metric alert

> Assuming the default `alertmanager.yaml` is used, alerts here will send a webhook call to the `example-app`. To view this, run `kubectl logs -f $(kubectl get pods | grep example-app | cut -f 1 -d ' ') | grep 'level=info'`

There are three custom metrics that the example application exposes.

To trigger the first two alerts, navigate to [http://localhost:4321/a/101](http://localhost:4321/a/101). To unset the alert, navigate to [http://localhost:4321/a/99](http://localhost:4321/a/99)

To trigger the third alert, navigate to [http://localhost:4321/b/42](http://localhost:4321/b/42). To unset the alert, navigate to [http://localhost:4321/b/41](http://localhost:4321/b/41)

To trigger the fourth alert, navigate to [http://localhost:4321/c/-123](http://localhost:4321/c/-123). To unset the alert, navigate to [http://localhost:4321/c/1](http://localhost:4321/c/1)

### Triggering a team alert

> In order for this to work on OpsGenie, you will need to login to OpsGenie, create a team (or access the dashboard of an existing one), create a Prometheus integration for that team, and paste the API key into the `api_key` field of the appropriate `receiver`. See [Integrating OpsGenie](#integrating-opsgenie) for more details.

Team alerts are triggerable by setting the `team_metric_[0123456]` metric. This can be done via the path `/{teamId}/{severityLevel}`.

For example:
1. To trigger an info level alert for team 0, call [http://localhost:4321/0/1](http://localhost:4321/0/1).
2. To trigger a critical level alert for team 0, call [http://localhost:4321/0/1](http://localhost:4321/0/5).
3. To trigger a medium level alert for team 1, call [http://localhost:4321/0/1](http://localhost:4321/1/3).

Levels are mapped as follows:

| Description | Level | OpsGenie Priority |
| --- | --- | --- |
| Info | 1 | P5 |
| Low | 2 | P4 |
| Medium | 3 | P3 |
| High | 4 | P2 |
| Critical | 5 | P1 |

## Integration Guides

### Integrating OpsGenie

1. Go to OpsGenie and create a new Team (or use an existing one)
2. Access the Team Dashboard and access **Integrations**
3. Add an integration for Prometheus and click Save
4. Copy the API key and use that in the `alertmanager`'s `receiver` configurations at `./config/alertmanager.yaml`
5. Replicate for as many teams as you have

> Note that using the API key from the main settings page will result in the message `To perform this action, use an API key from an API integration.` and using an API key that is completely wrong will result in the message `Key format is not valid!` or `Could not authenticate`

An overview of what the flow of data looks like:

```
*-----*                 *------------*
| app | <--/metrics---- | prometheus |
*-----*                 *------------*
                         |
                         | -- (PrometheusRule evaluation)
                         |
                         \-*---------------*
                           | alert-manager | ----------------\
                           *---------------*        |        |
                                                    |        |
  (AlertManager route.routes[*].match evaluation) --/        |
                                                             |
              *----------*                                   |
      /------ | opsgenie | <----https://api.opsgenie.com/v2/alerts
      |       *----------*     [header(Authorization: GenieKey XYZ)]
  *-------*
  | phone | (buzz buzz)
  | email |
  | slack |
  *-------*
```

## Common Issues

### ServiceMonitor not showing in Targets page

You can run the following Makefile recipe to verify the ServiceMonitor resource has been picked up:

```sh
make verify_servicemonitor_up;
```

The likely problem is not assigning the correct ServiceAccount to Prometheus such that Prometheus can access the pods.

### PrometheusRule - `groupname is repeated in the same file`

So apparently the same `groups.[*].name` cannot be repeated in the same `PrometheusRule` resource.

### AlertManager not sending alert

This has got to do with the `group_wait` and `group_interval` settings. Confirm that you have waited for at least the duration specified by the `group_interval` configuration. Note that this defaults to 10 minutes.

### OpsGenie alert not created

> This happens when the `group_by` AlertManager configuration groups only by `'job'` resulting in the same generated alert ID which is sent as the `alias` property to OpsGenie.

Verify first that AlertManager is sending an alert and OpsGenie is receiving it. This can be done by viewing the AlertManager logs (`kubectl logs -f alertmanager-alertmanager-0 alertmanager`) and checking for a `msg="Notify success"` string. On OpsGenie, visit the page at `https://${WORKSPACE_ID}.app.opsgenie.com/settings/log` and confirm that you see an entry with **Processed incomingData** for the data AlertManager is sending

For alerts that upgrade the priority (shifts it to a more serious alert), the alert will be upgraded. For alerts that downgrade the priority, the same does not happen and you will not be alerted on Slack if `send_resolved` is set to `false`. Setting `send_resolved` to `true` however creates a whole set of other problems that spams your channel. So pick your poison I guess.

## References

- [Prometheus Operator Repository](https://github.com/prometheus-operator/prometheus-operator/tree/master/Documentation)
- [Prometheus Operator Docs on Alerting](https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/user-guides/alerting.md)
- [AlertManager Configuration](https://prometheus.io/docs/alerting/latest/configuration/)
- [Prometheus Integration](https://docs.opsgenie.com/docs/prometheus-integration)
- [Slack App Integration](https://docs.opsgenie.com/docs/slack-app-integration)
- [How to set Opsgenie alert priority from Prometheus alert priority
](https://community.atlassian.com/t5/Opsgenie-questions/How-to-set-Opsgenie-alert-priority-from-Prometheus-alert/qaq-p/1211053)
- [Alertmanager + Notifications, how to notify only on certain alerts?](https://www.reddit.com/r/PrometheusMonitoring/comments/dmzm1k/alertmanager_notifications_how_to_notify_only_on/)

# Licensing

This POC is licensed under [the MIT license](./LICENSE).
