package main

import "github.com/sirupsen/logrus"

var log = logrus.New()

func init() {
	log.SetLevel(logrus.TraceLevel)
}

type CustomLoggingHandler struct {
	With func(f ...interface{})
}

func (c CustomLoggingHandler) Write(what []byte) (int, error) {
	c.With(string(what))
	return len(what), nil
}
