package main

import (
	"net/http"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	var handler http.Handler
	router := mux.NewRouter()
	// for webhook integration
	router.HandleFunc("/", WebhookHandler)
	// for opsgenie integration
	router.HandleFunc("/v2/alerts", WebhookHandler)
	// for modifying metrics
	router.HandleFunc("/{metric}/{howmuch}", func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		selectedMetric := params["metric"]
		amountToSet, atoiErr := strconv.Atoi(params["howmuch"])
		if atoiErr != nil {
			res := response{Data: atoiErr}
			res.Send(w, 400, "failed to convert '%s' to an integer", params["howmuch"])
			return
		}
		metricDictionary := map[string]prometheus.Gauge{
			"a": customMetricA,
			"b": customMetricB,
			"c": customMetricC,
			"0": metricForTeam0,
			"1": metricForTeam1,
			"2": metricForTeam2,
			"3": metricForTeam3,
			"4": metricForTeam4,
			"5": metricForTeam5,
			"6": metricForTeam6,
		}
		res := response{}
		if metric, ok := metricDictionary[selectedMetric]; ok {
			metric.Set(float64(amountToSet))
		} else {
			res.Send(w, http.StatusBadRequest, "no such metric '%s'", selectedMetric)
			return
		}
		res.Send(w, http.StatusOK, "set metric '%s' to %v", selectedMetric, amountToSet)
	})
	router.Handle("/metrics", promhttp.Handler())
	handler = handlers.LoggingHandler(CustomLoggingHandler{With: log.Trace}, router)
	log.Info("listening on 0.0.0.0:4321...")
	panic(http.ListenAndServe("0.0.0.0:4321", handler))
}
