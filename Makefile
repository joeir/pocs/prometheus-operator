NAME=pocpromop

# init recipes

up: precheck
	kind create cluster \
		--config ./kind-config.yml \
		--name $(NAME)

down:
	kind delete cluster --name $(NAME)

# setup recipes

alerts:
	kubectl config use-context kind-$(NAME)
	kubectl apply -f ./workloads/alerts.yml

alerts_down:
	kubectl config use-context kind-$(NAME)
	kubectl delete -f ./workloads/alerts.yml

example_app: example_app_image_in_cluster
	kubectl config use-context kind-$(NAME)
	kubectl apply -f ./workloads/app.yml

example_app_down:
	kubectl config use-context kind-$(NAME)
	kubectl delete -f ./workloads/app.yml

prometheus:
	kubectl config use-context kind-$(NAME)
	-kubectl delete secret alertmanager-alertmanager
	kubectl create secret generic alertmanager-alertmanager --from-file=./config/alertmanager.yaml
	kubectl apply -f ./workloads/prometheus.yml

prometheus_down:
	kubectl config use-context kind-$(NAME)
	kubectl delete -f ./workloads/prometheus.yml
	-kubectl delete secret alertmanager-alertmanager

prometheus_operator:
	kubectl config use-context kind-$(NAME)
	# if this next step fails, you need to run `git submodule init` first
	stat ./modules/prometheus-operator/bundle.yaml
	kubectl apply -f ./modules/prometheus-operator/bundle.yaml

prometheus_operator_down:
	kubectl config use-context kind-$(NAME)
	# if this next step fails, you need to run `git submodule init` first
	stat ./modules/prometheus-operator/bundle.yaml
	kubectl delete -f ./modules/prometheus-operator/bundle.yaml

# experimentation recipes

local:
	kubectl port-forward -n default svc/prometheus-operated 9090 \
		& kubectl port-forward -n default svc/alertmanager-operated 9093 \
		& kubectl port-forward svc/example-app 4321 \
		& wait

# utility recipes

alertmanager_config_base64:
	cat ./config/alertmanager.yaml | base64 -w 0

example_app_image:
	cd ./app && make image

example_app_image_in_cluster: example_app_image
	kubectl config use-context kind-$(NAME)
	kind load docker-image \
		stashaway/pocpromop:latest \
		--name $(NAME)

precheck:
	git --version
	docker --version
	kind --version
	kubectl version --client
	jq --version
	base64 --version

verify_servicemonitor_up:
	kubectl config use-context kind-$(NAME)
	kubectl get secret prometheus-prometheus -o json | jq -r '.data["prometheus.yaml.gz"]' | base64 -d | gunzip | grep example-app
