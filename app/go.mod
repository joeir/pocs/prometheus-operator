module gitlab.stashaway.com/joseph/poc-prometheus-operator/app

go 1.15

require (
	github.com/google/uuid v1.1.2
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.4.2
)
